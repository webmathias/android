package com.example.exemploservice;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class Service2 extends IntentService {

	public Service2() {
		super("Serviço 2 Exemplo");

	}
	
	
	@Override
	protected void onHandleIntent(Intent intent) {
		
		
		String urlPath = intent.getStringExtra("param1");
		Bundle extras = intent.getExtras();
		if (extras != null) {
			Messenger messenger = (Messenger) extras.get("MESSENGER");
			Message msg = Message.obtain();
			msg.arg1 = Activity.RESULT_OK;//"resultado";
			msg.obj = "Objeto de retorno";//output.getAbsolutePath();
			try {
				messenger.send(msg);
			} catch (android.os.RemoteException e1) {
				Log.w(getClass().getName(), "Exception sending message", e1);
			}

		}

	}

}

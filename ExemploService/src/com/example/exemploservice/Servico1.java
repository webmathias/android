package com.example.exemploservice;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class Servico1 extends Service {
	public Object controle = new Object();
	boolean rodando = true;
	@Override
	public void onDestroy() {
		rodando = false;
		super.onDestroy();
	}
	public int cont=0;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Executar código
		
		new Thread(){
			public void run(){
				
				//rodando = true;
				while (rodando) {
					synchronized (controle) {
						try {
							controle.wait(5000);
							Log.e("Executando", ""+cont++);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}.start();
	
		return super.onStartCommand(intent, flags, startId);
	}
	
	public class Servico1Binder extends Binder{
		public Servico1 getServico(){
			return Servico1.this;
		}
	}
	@Override
	public IBinder onBind(Intent intent) {
		return new Servico1Binder();
	}

}

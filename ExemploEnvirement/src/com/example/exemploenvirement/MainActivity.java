package com.example.exemploenvirement;

import java.io.IOException;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		String texto="";
		try {
			texto += "getExternalStorageState:"+Environment.getExternalStorageState()+"\n";
			texto += "getRootDirectory:"+Environment.getRootDirectory().getCanonicalPath()+"\n";
			texto += "getDataDirectory:"+Environment.getDataDirectory().getCanonicalPath()+"\n";
			texto += "getDownloadCacheDirectory:"+Environment.getDownloadCacheDirectory().getCanonicalPath()+"\n";
			texto += "getExternalStorageDirectory:"+Environment.getExternalStorageDirectory().getCanonicalPath()+"\n";
			texto += "DIRECTORY_ALARMS:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS).getCanonicalPath()+"\n";
			texto += "DIRECTORY_DCIM:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getCanonicalPath()+"\n";
			texto += "DIRECTORY_DOWNLOADS:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getCanonicalPath()+"\n";
			texto += "DIRECTORY_MOVIES:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getCanonicalPath()+"\n";
			texto += "DIRECTORY_MUSIC:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getCanonicalPath()+"\n";
			texto += "DIRECTORY_NOTIFICATIONS:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS).getCanonicalPath()+"\n";
			texto += "DIRECTORY_PICTURES:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getCanonicalPath()+"\n";
			texto += "DIRECTORY_PODCASTS:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS).getCanonicalPath()+"\n";
			texto += "DIRECTORY_RINGTONES:"+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES).getCanonicalPath()+"\n";
		 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TextView txt = (TextView)findViewById(R.id.text1);
		txt.setText(texto);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}

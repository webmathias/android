package com.example.exemplointentcomretorno;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void clickButton1(View v) {
		Intent i = new Intent(getApplicationContext(), Ativity2.class);
		i.putExtra("a", 5);
		i.putExtra("b", 6);
		startActivityForResult(i, 99);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			Toast.makeText(getApplicationContext(), "Retorno não OK",
					Toast.LENGTH_LONG).show();
			return;
		}
		switch (requestCode) {
		case 99:
			TextView txt = (TextView) findViewById(R.id.textView1);
			int retorno = data.getIntExtra("resultado", 0);
			txt.setText(retorno + "");
			break;
		case 100:
			 			
			Bitmap pic;
			try {
				android.provider.MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(file));
				pic = ThumbnailUtils.extractThumbnail(android.provider.MediaStore.Images.Media.getBitmap(
						getContentResolver(), Uri.fromFile(file)),800,600);
				 
				 
				Toast.makeText(getApplicationContext(),
						pic.getWidth() + "x" + pic.getHeight(),
						Toast.LENGTH_LONG).show();
				if (pic != null) { // Display your image in an ImageView in your
									// layout (if you want to test it)
					pic = trocaCor(pic);
					ImageView imageview1 = (ImageView) this
							.findViewById(R.id.imageView1);
					imageview1.setImageBitmap(pic);
					imageview1.invalidate();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	public Bitmap trocaCor(Bitmap img){
		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				int cor = img.getPixel(x, y);
				if(Color.red(cor) <100 
						&& Color.green(cor) <100 
						&& Color.blue(cor) <100){
					img.setPixel(x, y, Color.BLUE);
				}
			}
		}
		
		return img;
	}
	File file;

	public void camera(View v) {				 
		file = new File(Environment.getExternalStorageDirectory(),
				String.valueOf(System.currentTimeMillis()) + ".jpg");
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
		intent.putExtra("return-data", false);
		this.startActivityForResult(intent, 100);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}

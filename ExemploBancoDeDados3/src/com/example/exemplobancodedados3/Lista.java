package com.example.exemplobancodedados3;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Lista extends Activity {
  
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista);
    }
    public void atualizaLista(ArrayList<String[]> lista){
    	ListView listaview = (ListView) findViewById(R.id.ListView01);

    	listaview.setAdapter(new ListAdapter(getApplicationContext(),
				R.layout.linha, lista));    	
    }
    @Override
    protected void onStart() { 
    	super.onStart();
    	String[] col1 = getIntent().getExtras().getStringArray("col1");
    	String[] col2 = getIntent().getExtras().getStringArray("col2");
    	ArrayList<String[]> lista = new ArrayList<String[]>();
    	for (int i = 0; i < col1.length; i++) {
			lista.add(new String[]{col1[i],col2[i]});
		}
    	atualizaLista(lista);
    }
    
    public class ListAdapter extends ArrayAdapter<String[]> {

		private ArrayList<String[]> items;

		public ListAdapter(Context context, int textViewResourceId,
				ArrayList<String[]> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.linha, null);
			}
			String[] o = items.get(position);
			if (o != null) {
				TextView tt = (TextView) v.findViewById(R.id.TextView01);
				TextView bt = (TextView) v.findViewById(R.id.TextView02);

				if (tt != null) {
					tt.setText(o[0]);
				}
				if (bt != null) {
					bt.setText(o[1]);
				}
			}
			return v;
		}
	}
}
package com.example.exemplovideo1;

import android.app.Activity;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends Activity {
	VideoView mVideo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mVideo = (VideoView) findViewById(R.id.videoView1);
		mVideo.setMediaController(new MediaController(this));
		Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.trailer); 
		mVideo.setVideoURI(video);
		//Setar controle de audio padrão
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		// Sem titulo
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// FullScreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void start(View v) {
		mVideo.start();
	}

	public void pause(View v) {
		mVideo.pause();
	}

	public void stop(View v) {
		mVideo.suspend();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		stop(null);
		super.onPause();
	}

}
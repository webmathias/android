package com.example.exemplobluetooth;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
	int REQUEST_ENABLE_BT = 0x5;
	BluetoothAdapter mBluetoothAdapter;

	public void test(View v) {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		new AsyncTask<Void, Void, Void>() {
			protected void onPostExecute(Void result) {
				Toast.makeText(getApplicationContext(), "Acabou Test",
						Toast.LENGTH_SHORT).show();
			};

			@Override
			protected Void doInBackground(Void... params) {

				if (mBluetoothAdapter == null) {
					Toast.makeText(getApplicationContext(),
							"Bluetooth não suportado", Toast.LENGTH_LONG)
							.show();
					return null;
				}

				if (!mBluetoothAdapter.isEnabled()) {

					Intent enableBtIntent = new Intent(
							BluetoothAdapter.ACTION_REQUEST_ENABLE);

					startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
				}

				return null;
			}
		}.execute();
	}

	List<BluetoothDevice> devices = new LinkedList<BluetoothDevice>();

	public void list(View v) {
		new AsyncTask<Void, Void, Void>() {
			protected void onPostExecute(Void result) {
				Toast.makeText(getApplicationContext(), "Acabou List",
						Toast.LENGTH_SHORT).show();
				for (BluetoothDevice device : devices) {
					Toast.makeText(getApplicationContext(),
							"Device: " + device.getName(), Toast.LENGTH_SHORT)
							.show();
				}
			};

			@Override
			protected Void doInBackground(Void... params) {
				Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
						.getBondedDevices();
				if (pairedDevices.size() > 0) {
					for (BluetoothDevice device : pairedDevices) {
						devices.add(device);

					}
				}
				return null;
			}
		}.execute();
	}

	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				devices.add(device);
				Toast.makeText(getApplicationContext(),
						"Device: " + device.getName(), Toast.LENGTH_SHORT)
						.show();
			}
		}
	};

	public void scan(View v) {
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		registerReceiver(mReceiver, filter);
		mBluetoothAdapter.startDiscovery();

	}

	public void cancelscan(View v) {
		mBluetoothAdapter.cancelDiscovery();
		unregisterReceiver(mReceiver);
	}

	AsyncTask server;

	public void createServer(View v) {

		server = new AsyncTask<Void, BluetoothSocket, Void>() {
			protected void onProgressUpdate(BluetoothSocket[] values) {
				if (values.length == 0) {
					Toast.makeText(getApplicationContext(),
							"Servidor Criado Esperando por conexão",
							Toast.LENGTH_SHORT).show();
					return;
				}
				Toast.makeText(getApplicationContext(),
						"Conectado: " + values[0].getRemoteDevice().getName(),
						Toast.LENGTH_SHORT).show();
				try {
					values[0].close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};

			protected void onCancelled() {
				server = null;
				Toast.makeText(getApplicationContext(), "Servidor Desligado",
						Toast.LENGTH_SHORT).show();
			};

			@Override
			protected Void doInBackground(Void... params) {
				BluetoothServerSocket mmServerSocket = null;
				try {
					mmServerSocket = mBluetoothAdapter
							.listenUsingRfcommWithServiceRecord(
									"",
									UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
					publishProgress();
					while (server != null) {
						BluetoothSocket cliente = mmServerSocket.accept();
						publishProgress(cliente);
					}

					// cliente.close();
				} catch (Exception e) {

					e.printStackTrace();
				} finally {
					try {
						mmServerSocket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				return null;
			}
		}.execute();

	}

	public void cancelServer(View v) {
		server.cancel(true);
	}

	String[] deviceslist;

	public void conectar(View v) {
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int selected) {
				// cancelscan(null);
				Toast.makeText(getApplicationContext(),
						"Selected:" + devices.get(selected).getName(),
						Toast.LENGTH_SHORT).show();
				dialog.dismiss();
				BluetoothSocket mmSocket;
				try {
					mmSocket = devices
							.get(selected)
							.createRfcommSocketToServiceRecord(
									UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
					mmSocket.connect();
					mmSocket.close();
				} catch (IOException e) {
					Toast.makeText(getApplicationContext(),
							"Erros ao Conectar:" + e.getMessage(),
							Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}

			}
		};
		deviceslist = new String[devices.size()];
		for (int i = 0; i < devices.size(); i++) {
			BluetoothDevice device = devices.get(i);
			deviceslist[i] = device.getName();

		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecione cliente:").setSingleChoiceItems(
				deviceslist, 1, listener);
		builder.create().show();

	}

	public void emparelhar(View v) {
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int selected) {
				String ACTION_PAIRING_REQUEST = 
						"android.bluetooth.device.action.PAIRING_REQUEST";
				Intent intent = new Intent(ACTION_PAIRING_REQUEST);
				String EXTRA_DEVICE = 
						"android.bluetooth.device.extra.DEVICE";
				intent.putExtra(EXTRA_DEVICE, devices.get(selected));
				String EXTRA_PAIRING_VARIANT = "android.bluetooth.device.extra.PAIRING_VARIANT";
				int PAIRING_VARIANT_PIN = 0;
				intent.putExtra(EXTRA_PAIRING_VARIANT, PAIRING_VARIANT_PIN);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				dialog.dismiss();

			}
		};
		deviceslist = new String[devices.size()];
		for (int i = 0; i < devices.size(); i++) {
			BluetoothDevice device = devices.get(i);
			deviceslist[i] = device.getName();

		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecione cliente:").setSingleChoiceItems(
				deviceslist, 1, listener);
		builder.create().show();
		Intent discoverableIntent = new Intent(
				BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
		discoverableIntent.putExtra(
				BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
		startActivity(discoverableIntent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}

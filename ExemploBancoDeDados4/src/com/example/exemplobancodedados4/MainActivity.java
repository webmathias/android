package com.example.exemplobancodedados4;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void Filtrar(View v) {
		EditText entrada = (EditText) findViewById(R.id.editText1);
		String filtro = entrada.getText().toString();
		// Fazer consulta no banco
		Cursor cursor = banco.rawQuery();
		String from[] = {
				"id",
				"nome",
				"valor",
				"total"
		};
		int to[] = {
				R.id.textCampo1,
				R.id.textCampo2,
				R.id.textCampo3,
				R.id.textCampo4
		};
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.linha, cursor, from, to, 0);
		ListView lista = (ListView)findViewById(R.id.listView1);
		lista.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}

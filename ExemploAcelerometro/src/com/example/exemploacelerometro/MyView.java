package com.example.exemploacelerometro;

import android.app.Service;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MyView extends SurfaceView implements SurfaceHolder.Callback,
		SensorEventListener {
	SurfaceHolder h;

	public MyView(Context context) {
		super(context);
		h = getHolder();
		h.addCallback(this);
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {

	}

	boolean vivo = false;
	Thread t;
	float x = 200;
	float y = 200;
	float vx=0;
	float vy=0;
	float raio;
	SensorManager sm;
	Sensor s;

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		sm = (SensorManager) getContext().getSystemService(
				Service.SENSOR_SERVICE);
		s = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sm.registerListener(this, s, SensorManager.SENSOR_DELAY_GAME);

		t = new Thread() {
			@Override
			public void run() {
				vivo = true;
				while (vivo) {
					Canvas c = h.lockCanvas();
					if (c != null) {
						Paint p = new Paint();
						c.drawColor(Color.WHITE);
						p.setColor(Color.GREEN);
						c.drawCircle(x, y, raio, p);
						h.unlockCanvasAndPost(c);
						x += vx;
						y += vy;
					}
				}
			}
		};
		t.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		sm.unregisterListener(this);
		vivo = false;
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			vx = vx*0.8f + -event.values[0]*0.2f;
			vy = vy*0.8f + event.values[1]*0.2f;
			raio = event.values[2];
		}

	}

}

package com.example.exemplosurfaceview;

import java.io.IOException;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MySurfaceView extends SurfaceView implements
		SurfaceHolder.Callback {
	SurfaceHolder h;

	public MySurfaceView(Context context) {
		super(context);
		h = getHolder();
		h.addCallback(this);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		for (int i = 0; i < event.getPointerCount(); i++) {
			event.getX(i);
			event.getY(i);
		}
		return super.onTouchEvent(event);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	Thread t;
	boolean vivo = false;
	Bitmap img;

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		t = new Thread() {
			public void run() {

				AssetManager assetManager = getContext().getAssets();
				SpriteAnimada anim = null;
				try {
					img = BitmapFactory.decodeStream(assetManager
							.open("sprite1.png"));
					anim = new SpriteAnimada(
							BitmapFactory.decodeStream(assetManager
									.open("explosion-sprite.png")), 1000, 3, 5);

				} catch (IOException e) {

					e.printStackTrace();
				}
				int largura = img.getWidth() / 5;
				Rect r[] = new Rect[5];
				for (int i = 0; i < r.length; i++) {
					r[i] = new Rect(i * largura, 0, i * largura + largura,
							img.getHeight());
				}
				RectF dst = new RectF(0, 0, img.getWidth() / 4, img.getHeight());
				long time = System.currentTimeMillis();
				float fps;
				vivo = true;
				while (vivo) {
					Canvas c = h.lockCanvas();

					if (c != null) {
						c.drawColor(Color.WHITE);
						Paint p = new Paint();
						anim.draw(200, 200, c, p);
						c.drawBitmap(
								img,
								r[(int) ((System.currentTimeMillis() / 1000) % 5)],
								dst, p);
						p.setColor(Color.BLACK);
						c.drawText(
								"T:"
										+ (int) ((System.currentTimeMillis() / 1000) % 5),
								50, 50, p);
						
						anim.update(System.currentTimeMillis() - time);
						time = System.currentTimeMillis() - time;
						fps = 1000f / (float) time;

						time = System.currentTimeMillis();
						h.unlockCanvasAndPost(c);
					}
				}
			}
		};
		t.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		vivo = false;
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

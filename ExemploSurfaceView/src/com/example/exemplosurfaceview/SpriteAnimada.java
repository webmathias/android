package com.example.exemplosurfaceview;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class SpriteAnimada {
	Bitmap img;
	int tempo;
	int cols;
	int altura,largura, imagens;
	public SpriteAnimada(Bitmap img, int tempo, int rows, int cols) {
		super();
		this.img = img;
		this.tempo = tempo;
		altura = img.getHeight()/rows;
		largura = img.getWidth()/cols;
		imagens = cols*rows;
		this.cols = cols; 
	}
	Rect pos = new Rect();
	Rect dst = new Rect();
	int indice = 0;
	int time;
	public void update(long diftime){
		if(time > tempo/imagens){
			time = 0;
			indice=(indice+1)%imagens;
		}
		time += diftime;
		pos.left = (indice%cols)*largura;
		pos.top = (indice/cols)*altura;
		pos.right = pos.left+largura;
		pos.bottom = pos.top+altura;
	}
	public void draw(int x, int y, Canvas c, Paint p) {
		dst.left = x;
		dst.right=x+pos.width();
		dst.top = y;
		dst.bottom=y+pos.height();
		c.drawBitmap(img, pos, dst, p);
	}
}








package flock;

import java.util.LinkedList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import flock.*;

public class BirdSimulacao extends SurfaceView implements SurfaceHolder.Callback {

	public BirdSimulacao(Context context, AttributeSet attrs) {
		super(context, attrs);
		getHolder().addCallback(this);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub
		reset();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// reset();
		synchronized (flock) {
			Obstacle obs = new Obstacle((int) event.getX(), (int) event.getY());
			flock.addBird(obs);
		}

		return false;
	}

	boolean vivo;
	Thread t;

	Flock flock;
	long diftime;
	int numberOfGreenBirds;
	int numberOfBlueBirds;
	int numberOfRedPredators;
	int greenBirdSpeed;
	int blueBirdSpeed;
	int redPredatorSpeed;
	int greenBirdMaxTheta;
	int blueBirdMaxTheta;
	int redPredatorMaxTheta;
	int redPredatorHunger;
	int separateDistance;
	int detectDistance;
	final int DEFAULT_NUMBER_GREEN = 40;
	final int DEFAULT_NUMBER_BLUE = 50;
	final int DEFAULT_NUMBER_RED = 0;
	final int DEFAULT_GREEN_THETA = 15;
	final int DEFAULT_BLUE_THETA = 10;
	final int DEFAULT_RED_THETA = 20;
	final int DEFAULT_GREEN_SPEED = 5;
	final int DEFAULT_BLUE_SPEED = 6;
	final int DEFAULT_RED_SPEED = 7;
	final int DEFAULT_RED_HUNGER = 3;
	final int DEFAULT_OBSTACLE_SEPARATE = 30;
	final int DEFAULT_OBSTACLE_DETECT = 60;

	public void reset() {

		numberOfGreenBirds = DEFAULT_NUMBER_GREEN;
		numberOfBlueBirds = DEFAULT_NUMBER_BLUE;
		numberOfRedPredators = DEFAULT_NUMBER_RED;
		greenBirdMaxTheta = DEFAULT_GREEN_THETA;
		blueBirdMaxTheta = DEFAULT_BLUE_THETA;
		redPredatorMaxTheta = DEFAULT_RED_THETA;
		greenBirdSpeed = DEFAULT_GREEN_SPEED;
		blueBirdSpeed = DEFAULT_BLUE_SPEED;
		redPredatorSpeed = DEFAULT_RED_SPEED;
		redPredatorHunger = DEFAULT_RED_HUNGER;
		separateDistance = DEFAULT_OBSTACLE_SEPARATE;
		detectDistance = DEFAULT_OBSTACLE_DETECT;

		// setControlValues();

		Bird.SeparationRange = separateDistance;
		Bird.DetectionRange = detectDistance;

		flock = new Flock();

		Flock.SeparationRange = separateDistance;
		Flock.DetectionRange = detectDistance;

		for (int i = 0; i < numberOfGreenBirds; i++) {
			Bird bird = new Bird(Color.GREEN);
			bird.setSpeed(greenBirdSpeed);
			bird.setMaxTurnTheta(greenBirdMaxTheta);
			flock.addBird(bird);
		}
		for (int i = 0; i < numberOfBlueBirds; i++) {
			Bird bird = new Bird(Color.BLUE);
			bird.setSpeed(blueBirdSpeed);
			bird.setMaxTurnTheta(blueBirdMaxTheta);
			flock.addBird(bird);
		}
		for (int i = 0; i < numberOfRedPredators; i++) {
			Predator bird = new Predator();
			bird.setSpeed(redPredatorSpeed);
			bird.setMaxTurnTheta(redPredatorMaxTheta);
			bird.setHunger(redPredatorHunger);
			flock.addBird(bird);
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Bird.setMapSize(getWidth(), getHeight());
		Flock.setMapSize(getWidth(), getHeight());
		reset();

		t = new Thread() {
			public void run() {

				vivo = true;
				Paint p = new Paint();
				SurfaceHolder h = getHolder();
				while (vivo) {

					long time = System.currentTimeMillis();
					Canvas c = h.lockCanvas();
					if (c != null) {

						synchronized (flock) {
							LinkedList<Bird> removedBirds = flock.move();
							c.drawColor(Color.WHITE);
							flock.draw(c);
						}
						p.setColor(Color.BLACK);
						c.drawText("Diftime:" + diftime, 50, 50, p);
						c.drawText("Birds:" + flock.birds.size(), 50, 100, p);
						h.unlockCanvasAndPost(c);
						long time1 = System.currentTimeMillis();
						diftime = time1 - time;
					}

				}

			};
		};
		t.start();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		vivo = false;
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

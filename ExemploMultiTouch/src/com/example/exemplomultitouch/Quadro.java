package com.example.exemplomultitouch;

import java.util.LinkedList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class Quadro extends View {

	public Quadro(Context context, AttributeSet attrs) {
		super(context, attrs);

	}
	LinkedList<PointF> pontos1 = new LinkedList<PointF>();
	LinkedList<PointF> pontos2 = new LinkedList<PointF>();
//	float x;
//	float y;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
//		x = event.getX();
//		y = event.getY();
		if(event.getPointerCount() == 1){
			pontos1.add(new PointF(event.getX()
					,event.getY()));
		}
		if(event.getPointerCount() == 2){
			pontos1.add(new PointF(event.getX(0)
					,event.getY(0)));
			pontos2.add(new PointF(event.getX(1)
					,event.getY(1)));
		}
	
		invalidate();
		return true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Paint p = new Paint();
		p.setAntiAlias(true);
		p.setFilterBitmap(true);
		// p.setColor(Color.argb(alpha, red, green, blue))
		p.setColor(Color.RED);
		p.setStrokeWidth(5);
		for (int i = 1; i < pontos1.size(); i++) {
			float x1 = pontos1.get(i-1).x;
			float y1 = pontos1.get(i-1).y;
			float x2 = pontos1.get(i).x;
			float y2 = pontos1.get(i).y;
			canvas.drawLine(x1, y1, x2, y2, p);
			p.setTextSize(20);
			canvas.drawText("Size:"+pontos1.size(),
					10, 50, p);
		}
		for (int i = 1; i < pontos2.size(); i++) {
			float x1 = pontos2.get(i-1).x;
			float y1 = pontos2.get(i-1).y;
			float x2 = pontos2.get(i).x;
			float y2 = pontos2.get(i).y;
			canvas.drawLine(x1, y1, x2, y2, p);
			p.setTextSize(20);
			canvas.drawText("Size:"+pontos2.size(),
					10, 50, p);
		}

	}

}

package com.example.exemploconsumocontentprovider;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		TextView v = (TextView)findViewById(R.id.text1);
		String texto="";
		Cursor c = getContentResolver().query(Uri.parse("content://com.example.exemplocontentprovider/notas1"), null, null, null, null);
		c.moveToFirst();
		do{
			texto += c.getString(0);
		}while(c.moveToNext());
		v.setText(texto);
		c.close();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}

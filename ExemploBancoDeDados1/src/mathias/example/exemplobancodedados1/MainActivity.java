package mathias.example.exemplobancodedados1;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	public String DB_PATH;

	class selectBanco extends AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... params) {
			String retorno = "";
			SQLiteDatabase db = null;
			try {
				db = SQLiteDatabase.openDatabase(DB_PATH, null,
						SQLiteDatabase.OPEN_READWRITE);
				db.execSQL("INSERT INTO Produto	" +
						"( id, nome) VALUES " +
						"( 2, \"Toalha De Rosto\" )");
				
//				db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)
//				ContentValues valores = new ContentValues();
//				valores.put("nome", "Mathias");
//				db.insert(table, nullColumnHack, valores)
//				db.delete(table, whereClause, whereArgs)
				Cursor c = db.rawQuery("select nome from Produto", null);
				if (c != null) {
					c.moveToFirst();
						
//					c.moveToFirst();
					while (c.moveToNext()) {
						int indice = c.getColumnIndex("nome")
						retorno += c.getString(0)+"\n";
					}
				}

			} catch (SQLiteException e) {
				retorno = "ERRO:"+e.getMessage() +" - "+DB_PATH;
			} finally {
				if (db != null)
					db.close();
			}

			return retorno;
		}
		@Override
		protected void onPostExecute(String result) {
			((TextView)findViewById(R.id.texto1)).setText(result);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		DB_PATH = Environment
				.getExternalStoragePublicDirectory(
						Environment.DIRECTORY_DOWNLOADS)
				+ "/exemplo5.db";
		new selectBanco().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}

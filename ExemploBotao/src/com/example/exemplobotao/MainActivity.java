package com.example.exemplobotao;

import android.os.Bundle;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button bt1 = (Button) findViewById(R.id.button1);
		if (bt1 != null)
			bt1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// v.animate().translationX(200);
//					float dest = 0;
//					dest = 360;
//					if (v.getRotation() == 360) {
//						dest = 0;
//					}
//					ObjectAnimator animation1 = ObjectAnimator.ofFloat(v,
//							"rotation", dest);
//					animation1.setDuration(2000);
//					animation1.start();
					v.animate().alpha(1);
					// Show how to load an animation from XML
					// Animation animation1 = AnimationUtils.loadAnimation(this,
					// R.anim.myanimation);
					// animation1.setAnimationListener(this);
					// animatedView1.startAnimation(animation1);

				}
			});

		Switch btonoff = (Switch) findViewById(R.id.botaoonoff);
		if (btonoff != null) {
			btonoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton arg0,
						boolean selected) {
					

				}
			});
		}
	}

	public void clickBotao2(View v) {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
